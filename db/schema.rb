# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_23_200807) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "coordinators", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "distributions", force: :cascade do |t|
    t.bigint "item_id"
    t.bigint "coordinator_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["coordinator_id"], name: "index_distributions_on_coordinator_id"
    t.index ["item_id"], name: "index_distributions_on_item_id"
  end

  create_table "item_types", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "items", force: :cascade do |t|
    t.bigint "item_type_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_type_id"], name: "index_items_on_item_type_id"
  end

  create_table "lots", force: :cascade do |t|
    t.bigint "item_id"
    t.string "origin"
    t.bigint "politic_id"
    t.integer "initial_quantity"
    t.integer "current_quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_lots_on_item_id"
    t.index ["politic_id"], name: "index_lots_on_politic_id"
  end

  create_table "politics", force: :cascade do |t|
    t.string "name"
    t.string "phone"
    t.string "political_party"
    t.string "legend"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "distributions", "coordinators"
  add_foreign_key "distributions", "items"
  add_foreign_key "items", "item_types"
  add_foreign_key "lots", "items"
  add_foreign_key "lots", "politics"
end
