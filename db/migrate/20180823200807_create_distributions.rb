class CreateDistributions < ActiveRecord::Migration[5.2]
  def change
    create_table :distributions do |t|
      t.references :item, foreign_key: true
      t.integer :quantity
      t.references :coordinator, foreign_key: true
      t.references :lot, foreign_key: true

      t.timestamps
    end
  end
end
