class CreatePolitics < ActiveRecord::Migration[5.2]
  def change
    create_table :politics do |t|
      t.string :name
      t.string :phone
      t.string :political_party
      t.string :legend

      t.timestamps
    end
  end
end
