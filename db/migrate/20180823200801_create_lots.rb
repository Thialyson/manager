class CreateLots < ActiveRecord::Migration[5.2]
  def change
    create_table :lots do |t|
      t.references :item, foreign_key: true
      t.string :origin
      t.references :politic, foreign_key: true
      t.integer :initial_quantity
      t.integer :current_quantity

      t.timestamps
    end
  end
end
