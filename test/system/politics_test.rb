require "application_system_test_case"

class PoliticsTest < ApplicationSystemTestCase
  setup do
    @politic = politics(:one)
  end

  test "visiting the index" do
    visit politics_url
    assert_selector "h1", text: "Politics"
  end

  test "creating a Politic" do
    visit politics_url
    click_on "New Politic"

    fill_in "Legend", with: @politic.legend
    fill_in "Name", with: @politic.name
    fill_in "Phone", with: @politic.phone
    fill_in "Political Party", with: @politic.political_party
    click_on "Create Politic"

    assert_text "Politic was successfully created"
    click_on "Back"
  end

  test "updating a Politic" do
    visit politics_url
    click_on "Edit", match: :first

    fill_in "Legend", with: @politic.legend
    fill_in "Name", with: @politic.name
    fill_in "Phone", with: @politic.phone
    fill_in "Political Party", with: @politic.political_party
    click_on "Update Politic"

    assert_text "Politic was successfully updated"
    click_on "Back"
  end

  test "destroying a Politic" do
    visit politics_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Politic was successfully destroyed"
  end
end
