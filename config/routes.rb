Rails.application.routes.draw do
  resources :distributions
  resources :lots
  resources :items
  resources :item_types
  resources :coordinators
  resources :politics
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
