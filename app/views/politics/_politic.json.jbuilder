json.extract! politic, :id, :name, :phone, :political_party, :legend, :created_at, :updated_at
json.url politic_url(politic, format: :json)
