json.extract! distribution, :id, :item_id, :coordinator_id, :created_at, :updated_at
json.url distribution_url(distribution, format: :json)
