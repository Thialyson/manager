json.extract! lot, :id, :item_id, :origin, :initial_quantity, :current_quantity, :created_at, :updated_at
json.url lot_url(lot, format: :json)
