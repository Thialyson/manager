json.extract! item, :id, :item_type_id, :politic_id, :quantity, :created_at, :updated_at
json.url item_url(item, format: :json)
