class Distribution < ApplicationRecord
  has_many :item
  belongs_to :coordinator
  belongs_to :lot
end
